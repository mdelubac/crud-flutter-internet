import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/service/role_repository.dart';
import 'package:linking_pictures/viewModel/list_role_state_notifier.dart';
import 'package:linking_pictures/viewModel/role_providers.dart';
import 'package:linking_pictures/viewModel/role_change_notifier.dart';
import '../service_imp/fake_role_repository.dart';

// NEEDS TO IMPROVE !!!

void main() {
  group('test ListRoleNotifier', () {
    test('Test method createRole()', () async {
      final container = ProviderContainer(
        overrides: [
          // Override the behavior of roleServiceProvider to return
          // FakeRoleService instead of RoleService.
          roleRepositoryProvider
              .overrideWithProvider(Provider((ref) => FakeRoleRepository()))
          // We do not have to override `roleViewControllerProvider`, it will automatically
          // use the overriden roleServiceProvider
        ],
      );
      addTearDown(container.dispose);

      //Before creating a role _________________________________
      final initialFutureRoleStateProviderState = await container.read(futureRoleStateProvider).state;
      expect(initialFutureRoleStateProviderState, null);

      //Creating a role from data entered in form _________________________________
      final roleNameEntered = TextEditingController();
      final descriptionEntered = TextEditingController();
      final roleCreatedDone = await container.read(listRoleProvider.notifier).createRole(roleNameEntered, descriptionEntered);

      //Check futureRoleStateProvider States _____________________________________
      final uncompletedFutureRoleStateProvider = container.read(futureRoleStateProvider).state;
      expect(uncompletedFutureRoleStateProvider, isA<Future<Role>>());

      final completedFutureRoleStateProviderState = await container.read(futureRoleStateProvider).state;
      expect(completedFutureRoleStateProviderState, isA<Role>());
      expect(completedFutureRoleStateProviderState!.roleName, 'roleNameEntered');
      expect(completedFutureRoleStateProviderState.description, 'descriptionEntered');
      expect(completedFutureRoleStateProviderState.id, 1);
    });


    test('Test method updateRole', () async {
      final container = ProviderContainer(
        overrides: [
          // Override the behavior of roleServiceProvider to return
          // FakeRoleService instead of RoleService.
          roleRepositoryProvider
              .overrideWithProvider(Provider((ref) => FakeRoleRepository()))
          // We do not have to override `roleViewControllerProvider`, it will automatically
          // use the overriden roleServiceProvider
        ],
      );
      addTearDown(container.dispose);

      //Check futureRoleStateProvider state before updating a role _________________________________
      final initialFutureRoleStateProviderState = await container.read(futureRoleStateProvider).state;
      expect(initialFutureRoleStateProviderState, null);

      //Updating a role from form_______________________________
      RoleChangeNotifier roleToUpdate = RoleChangeNotifier('Admin', 'CRUD', 2);
      final roleNameUpdated = TextEditingController();
      final descriptionUpdated = TextEditingController();

      final roleUpdatedDone = await container.read(listRoleProvider.notifier).updateRole(roleNameUpdated, descriptionUpdated, roleToUpdate);

      //Check futureRoleStateProvider states  _____________________________________
      final uncompletedFutureRoleStateProvider = container.read(futureRoleStateProvider).state;
      expect(uncompletedFutureRoleStateProvider, isA<Future<Role>>());


      final completedFutureRoleStateProviderState = await container.read(futureRoleStateProvider).state;
      expect(completedFutureRoleStateProviderState, isA<Role>());
      expect(completedFutureRoleStateProviderState!.roleName, 'roleNameUpdated');
      expect(completedFutureRoleStateProviderState.description, 'descriptionUpdated');
      expect(completedFutureRoleStateProviderState.id, 2);
    });

    
    test('Test method  fetchAllRoles()', () async{
      final container = ProviderContainer(
        overrides: [
          // Override the behavior of roleServiceProvider to return
          // FakeRoleRepository instead of RoleService.
          roleRepositoryProvider.overrideWithProvider(Provider((ref)=>FakeRoleRepository()))
      // We do not have to override `roleViewControllerProvider`, it will automatically
      // use the overriden roleServiceProvider
        ],
      );
      addTearDown(container.dispose);

      //I don't need to call fetchAllRoles() to check State? Need to understand why.

      //Check futureListRoleStateProvider states  _____________________________________
      final uncompletedFutureRolesStateProvider = container.read(listRoleProvider);
      expect(uncompletedFutureRolesStateProvider, isA<Future<List<Role>>>());

      final completedFutureRolesStateProviderState = await container.read(listRoleProvider);
      expect(completedFutureRolesStateProviderState.first,
          isA<Role>()
              .having((s) => s.roleName, 'roleName', 'User')
              .having((s) => s.description, 'description', 'Read Only')
              .having((s) => s.id, 'id', 1)
      );

    });

    test('Test method deleteRole()', () async {
      final container = ProviderContainer(
        overrides: [
          // Override the behavior of roleServiceProvider to return
          // FakeRoleService instead of RoleService.
          roleRepositoryProvider
              .overrideWithProvider(Provider((ref) => FakeRoleRepository()))
          // We do not have to override `roleViewControllerProvider`, it will automatically
          // use the overriden roleServiceProvider
        ],
      );
      addTearDown(container.dispose);

      //Before deleting a role _________________________________
      final initialFutureRolesStateProviderState = await container.read(listRoleProvider);
      expect(initialFutureRolesStateProviderState, isA<List<Role>>());

      Role roleToDelete = Role('nameToDelete', 'description to delete', 1);
      final roleDeleted = await container.read(listRoleProvider.notifier).deleteRole(roleToDelete);


      //Check futureRolesStateProvider states  _____________________________________
      final uncompletedFutureRolesStateProvider = container.read(listRoleProvider);
      expect(uncompletedFutureRolesStateProvider, isA<Future<List<Role>>>());

      final completedFutureRolesStateProviderState = await container.read(listRoleProvider);
      expect(completedFutureRolesStateProviderState.first,
          isA<Role>()
              .having((s) => s.roleName, 'roleName', 'User')
              .having((s) => s.description, 'description', 'Read Only')
              .having((s) => s.id, 'id', 1)
      );

    });

  });



}
