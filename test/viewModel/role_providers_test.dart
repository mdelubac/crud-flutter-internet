import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/viewModel/role_providers.dart';
import 'package:linking_pictures/viewModel/role_change_notifier.dart';
import 'package:mockito/mockito.dart';

class Listener extends Mock {
  void call(bool value);
}

void main() {

  //NEED TO IMPROVE !!!
  group('State Providers', () {
    test('test editModeStateProvider', () {
      // An object that will allow us to read providers
      // Do not share this between tests.
      final container = ProviderContainer();
      addTearDown(container.dispose);
      final listener = Listener();

      //Observe a provider and spy the changes.
      container.listen<StateController<bool>>(
        editModeStateProvider,
        didChange: (sub) => listener(sub.read().state),
      );

      // If role is null: editModeStateProvider is false and should notify listeners
      container.read(myRoleNotifierProvider).nullRole(RoleChangeNotifier('Marc', 'Fort', 1));
      // Why this line of code is need to notify listeners ?
      container.read(editModeStateProvider).state;
      verify(listener(false)).called(1);
      verifyNoMoreInteractions(listener);

      // If role is not null: editModeStateProvider true  and should notify listeners
      container
          .read(myRoleNotifierProvider)
          .selectedRole(Role('Marc', 'Fort', 1));
      container.read(editModeStateProvider).state;
      verify(listener(true)).called(1);
      verifyNoMoreInteractions(listener);
    });
    test('test myRoleNotifierProvider', () {
      // An object that will allow us to read providers
      // Do not share this between tests.
      final container = ProviderContainer();
      addTearDown(container.dispose);

      //check that selectedRole notify
      //SHOUD BE DONE FOR ALL LISTENERS
    });
  });


}

