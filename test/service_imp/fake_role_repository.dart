import 'package:http/http.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/service_imp/role_api_provider.dart';
import 'package:linking_pictures/service_imp/role_repository_imp.dart';


class FakeRoleRepository implements RoleRepositoryImp {

  @override
  Future<Role> createRole(Role role) async {
    return Role('roleNameEntered', 'descriptionEntered', 1);
  }

  @override
  Future<Role> updateRole(Role role) async {
    return Role('roleNameUpdated', 'descriptionUpdated', 2);
  }

  @override
  List<Role> rolesFromJson(String jsonString) {
    // TODO: implement rolesFromJson
    throw UnimplementedError();
  }

  @override
  Future<List<Role>> fetchAllRoles({Client? client}) async {
    Role role1 = Role('User', 'Read Only', 1);
    Role role2 = Role('Admin', 'CRUD', 2);
    var listOfRoles = <Role>[role1,role2];
    print ('Fake fetchAllRoles called');
    return listOfRoles;

  }

  @override
  Future<Role?> deleteRole(int id) async {
    return null;
  }

  @override
  // TODO: implement roleApiProvider
  RoleApiProvider get roleApiProvider => throw UnimplementedError();

  @override
  late Client client;

  @override
  // TODO: implement read
  get read => throw UnimplementedError();

}