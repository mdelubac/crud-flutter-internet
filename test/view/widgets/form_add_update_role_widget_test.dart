import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/service/role_repository.dart';
import 'package:linking_pictures/view/widgets/form_add_update_role_widget.dart';
import 'package:linking_pictures/viewModel/role_providers.dart';
import 'package:linking_pictures/viewModel/role_change_notifier.dart';
import '../../service_imp/fake_role_repository.dart';

void main() {

  testWidgets('User first arrive on form, enter and submit a Role',
      (WidgetTester tester) async {
    //Create widget to add a Role
    await tester.pumpWidget(
      ProviderScope(
        overrides: [
          roleRepositoryProvider.overrideWithProvider(
            Provider((ref) => FakeRoleRepository()),
          ),
        ],
        child: MaterialApp(
          home: Scaffold(
            body: FormAddOrUpdateRole(),
          ),
        ),
      ),
    );

    //Expect that all the elements of an empty form are present.
    expect(find.byType(TextFormField), findsNWidgets(2));
    expect(find.byType(ElevatedButton), findsOneWidget);
    expect(find.widgetWithText(ElevatedButton, 'Submit'), findsOneWidget);

    //Expect not to find Update button
    expect(find.widgetWithText(ElevatedButton, 'Update'), findsNothing);

    //User enters a role and submit
    final Finder roleName = find.widgetWithText(TextFormField, 'Role Name');
    final Finder roleDescription =
        find.widgetWithText(TextFormField, 'Role Discription');
    await tester.enterText(roleName, 'roleNameEntered');
    await tester.enterText(roleDescription, 'roleDescriptionEntered');
    await tester.tap(find.widgetWithText(ElevatedButton, 'Submit'));

    //Create new widget after Submit
    await tester.pumpAndSettle();

    //Verify all elements are present after submission
    expect(find.byType(Icon), findsOneWidget);
    expect(find.text('roleNameEntered Saved'), findsOneWidget);
  });

  testWidgets('User arrives on update form and update a role', (WidgetTester tester) async {
    //Create widget with selected role to update
    await tester.pumpWidget(
      ProviderScope(
        overrides: [
          //Fake that a role was selected to be updated
          myRoleNotifierProvider.overrideWithValue(
            RoleChangeNotifier('Admin', 'CRUD', 2),
          ),
          //Fake http RoleService
          roleRepositoryProvider
              .overrideWithProvider(Provider((ref) => FakeRoleRepository())),
        ],
        child: MaterialApp(
          home: Scaffold(
            body: FormAddOrUpdateRole(),
          ),
        ),
      ),
    );

    //Verify that all the components are present
    expect(find.widgetWithText(TextFormField, 'Admin'), findsOneWidget);
    expect(find.widgetWithText(TextFormField, 'CRUD'), findsOneWidget);
    expect(find.widgetWithText(ElevatedButton, 'Update'), findsOneWidget);

    //Verify that submit button is not present
    expect(find.widgetWithText(ElevatedButton, 'Submit'), findsNothing);

    // User change values
    final Finder roleName = find.widgetWithText(TextFormField, 'Admin');
    final Finder roleDescription = find.widgetWithText(TextFormField, 'CRUD');

    await tester.enterText(roleName, 'roleNameUpdated');
    await tester.enterText(roleDescription, 'descriptionUpdated');

    await tester.tap(find.widgetWithText(ElevatedButton, 'Update'));

    //Create new widget after Submit
    await tester.pumpAndSettle();

    //Verify all elements are present after submission
    expect(find.byType(Icon), findsOneWidget);
    expect(find.text('roleNameUpdated Updated'), findsOneWidget);

  });




}
