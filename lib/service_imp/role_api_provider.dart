import 'dart:convert';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/service_imp/url.dart';

final roleApiProvider = Provider<RoleApiProvider>((ref)=>RoleApiProvider());

class RoleApiProvider {


  http.Client client = http.Client();

  Future<Role> createRole(Role role) async {
    final http.Response response = await client.post(Uri.parse(URL.ADD_URL),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(role));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return Role.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load a role');
    }
  }

  Future<Role> updateRole(Role role) async {
    final http.Response response = await client.put(
      Uri.parse(URL.UPD_URL),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(role),
    );
    if (response.statusCode == 200) {
      return Role.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to update Role.');
    }
  }

  //Optional parameter {http.Client client} needed for testing
  List<Role> rolesFromJson(String jsonString) {
    final data = jsonDecode(jsonString);
    return List<Role>.from(data.map((item) => Role.fromJson(item)));
  }

  Future<List<Role>> fetchAllRoles() async {
    final response = await client.get(Uri.parse(URL.GET_URL));
    if (response.statusCode == 200) {
      return rolesFromJson(response.body);
    } else {
      throw Exception('Failed to load all roles');
    }
  }

  Future<Role?> deleteRole(int id) async {
    final http.Response response = await client.delete(
      Uri.parse('${URL.DEL_URL}/$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    try{
      if (response.statusCode == 200) {
        return Role.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Failed to delete role');
      }
    }catch(e){
      print('Exception at class roleApiProvider => Method  deleteRole: $e');
    }

  }
}
