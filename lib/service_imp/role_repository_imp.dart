
import 'package:hooks_riverpod/all.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/service/role_repository.dart';
import 'package:linking_pictures/service_imp/role_api_provider.dart';


class RoleRepositoryImp implements RoleRepository{
  RoleRepositoryImp(this.read);

  final Reader read;


  @override
  Future<Role> createRole(Role role) => read(roleApiProvider).createRole(role);

  @override
  Future<Role> updateRole(Role role) => read(roleApiProvider).updateRole(role);

  @override
  Future<List<Role>> fetchAllRoles() => read(roleApiProvider).fetchAllRoles();

  @override
  Future<Role?> deleteRole(int id) => read(roleApiProvider).deleteRole(id);



  }
