import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'role.g.dart';
@JsonSerializable()

  class Role extends Equatable{

  final int? id;
  final String? roleName;
  final String? description;

  const Role([this.roleName, this.description, this.id]);


  @override
  // TODO: implement props
  List<Object?> get props => [id, roleName, description];

  factory Role.fromJson(Map<String, dynamic> json) => _$RoleFromJson(json);

  Map<String, dynamic> toJson() => _$RoleToJson(this);


  }









