import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/service_imp/role_repository_imp.dart';

final roleRepositoryProvider = Provider<RoleRepository>((ref)=> RoleRepositoryImp(ref.read));

abstract class RoleRepository{

  Future<Role> createRole(Role role);
  Future<Role> updateRole(Role role);
  Future<List<Role>> fetchAllRoles();
  Future<Role?> deleteRole(int id);


}