import 'package:flutter/cupertino.dart';
import 'package:linking_pictures/model/role.dart';

// This Notifier is for navigation between and input a selected
//role into the form fields
class RoleChangeNotifier extends ChangeNotifier{

  int? id;
  String? roleName;
  String? description;

  RoleChangeNotifier([this.roleName, this.description, this.id]);

  void selectedRole(Role role){
    id =role.id;
    roleName=role.roleName;
    description=role.description;
    notifyListeners();
  }

  void nullRole(RoleChangeNotifier? role){
    id=null;
    roleName =null;
    description = null;
    notifyListeners();
  }


}