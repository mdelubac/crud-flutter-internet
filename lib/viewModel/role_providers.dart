import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/viewModel/list_role_state_notifier.dart';
import 'package:linking_pictures/viewModel/role_change_notifier.dart';

// Notify when a role is null or is selected
final myRoleNotifierProvider =
    ChangeNotifierProvider.autoDispose<RoleChangeNotifier>((ref) {
  return RoleChangeNotifier();
});





//Provides a Future<Role> role for FutureBuilder<Role> if role is add or updated in database
final futureRoleStateProvider =
    StateProvider.autoDispose<Future<Role>?>((ProviderReference ref) => null);

//Provides bool to switch to add or update mode
final editModeStateProvider =
    StateProvider.autoDispose<bool>((ref) {
  final _selectedRole = ref.watch(myRoleNotifierProvider);
  if (_selectedRole.id != null) {
    return true;
  } else {
    return false;
  }
});



