import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/service/role_repository.dart';
import 'package:linking_pictures/viewModel/role_change_notifier.dart';
import 'package:linking_pictures/viewModel/role_providers.dart';

final listRoleProvider =
StateNotifierProvider.autoDispose<ListRoleNotifier, Future<List<Role>>>(
        (ref) => ListRoleNotifier(ref.read));

class ListRoleNotifier extends StateNotifier<Future<List<Role>>> {
  ListRoleNotifier(this.read)
      : super(read(roleRepositoryProvider).fetchAllRoles());

  Reader read;

  Future<void> fetchAllRoles() async {

    final allRolesFetched = await read(roleRepositoryProvider).fetchAllRoles();
    try{
      if (mounted) {
        state = Future.delayed(Duration.zero, () => allRolesFetched);
      }
      /*
      Need to make an exceptionProvider to show users the exception in snack bar.
      Also make a customException that implements Exception for more user friendly error
      for all CRUD methods
       */
    }catch (e){
      print('Exception at class ListRoleNotifier => Method fetchAllRoles: $e');
    }

  }

  Future<void> createRole(TextEditingController _roleName,
      TextEditingController _description) async {

    final roleCreated = await read(roleRepositoryProvider)
        .createRole(Role(_roleName.text, _description.text));
    try{
      if (mounted) {
        List<Role> list = await state;
        state = Future.delayed(Duration.zero, () => [roleCreated, ...list]);
      }
      read(futureRoleStateProvider).state =
          Future.delayed(Duration(milliseconds: 0), () => roleCreated);

    }catch(e){
      print('Exception at class ListRoleNotifier => Method createRole: $e');
    }
  }


  Future<void> updateRole(TextEditingController _roleName,
      TextEditingController _description, RoleChangeNotifier role) async {

    final roleUpdated = await read(roleRepositoryProvider)
        .updateRole(Role(_roleName.text, _description.text, role.id));
    try {
      if (mounted) {
        List<Role> list = await state;
        state = Future.delayed(
          Duration.zero,
              () => [for(final roleInList in list )
            if(roleInList.id == roleUpdated.id) roleUpdated else roleInList],
        );
      }
      read(futureRoleStateProvider).state =
          Future.delayed(Duration.zero, () => roleUpdated);
    } catch(e){
      print('Exception at class ListRoleNotifier => Method updateRole: $e');
    }

  }

  Future<void> deleteRole(Role role) async {

    await read(roleRepositoryProvider).deleteRole(role.id!);
    try{
      if (mounted) {
        List<Role> list = await state;
        state = Future.delayed(
          Duration.zero,
              () => [for(final roleInList in list )
            if(roleInList.id != role.id) roleInList],
        );
      }
    } catch (e){
      print('Exception at class ListRoleNotifier => Method  deleteRole: $e');
    }

  }
  void addSelectedRoleInFormFields(TextEditingController _roleName,
      TextEditingController _description, RoleChangeNotifier role) {
    try {
      if (role.id != null) {
        _roleName.text = role.roleName!;
        _description.text = role.description!;
      }
    }catch(e){
      print('Exception at class ListRoleNotifier => Method  addSelectedRoleInFormFields: $e');
    }
  }

}
