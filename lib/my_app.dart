import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/view/screens/edit_role_screen.dart';
import 'package:linking_pictures/view/screens/homepage_screen.dart';

import 'package:linking_pictures/viewModel/role_providers.dart';
import 'package:linking_pictures/viewModel/role_change_notifier.dart';


class MyApp extends HookWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    RoleChangeNotifier? _selectedRole = useProvider(myRoleNotifierProvider);

    return MaterialApp(
      title: 'Test CRUD and Navigation 2',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Navigator(
        pages: [
          MaterialPage(
            key: ValueKey('Home'),
            child: MyHomePage(),
          ),

          if (_selectedRole?.id !=null)
            MaterialPage(
              key: ValueKey('EditRole'),
              child: EditRoleScreen(),
            ),
        ],
        onPopPage: (route, result) {
          if (!route.didPop(result)) return false;

          context.read(myRoleNotifierProvider).nullRole(_selectedRole);

          /* Not needed because of autodispose ?
          THIS IS NEED need to reset to null: Find a more elegant way  ? need to pur in role view ?
          context.read(myRoleNotifierProvider).nullRole(_selectedRole);
           */
          return true;
        },
      ),
    );
  }
}
