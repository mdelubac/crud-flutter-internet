import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/viewModel/list_role_state_notifier.dart';
import 'package:linking_pictures/viewModel/role_providers.dart';
import 'package:linking_pictures/viewModel/role_change_notifier.dart';


// ignore: must_be_immutable
class FormAddOrUpdateRole extends HookWidget {

  final TextEditingController _roleName = TextEditingController();
  final TextEditingController _description = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Widget _buildRoleName() {
    return TextFormField(
      controller: _roleName,
      decoration: InputDecoration(
        hintText: 'Role Name',
        border: const OutlineInputBorder(),
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Please enter your name';
        }
        return null;
      },
    );
  }

  Widget _buildDescription() {
    return TextFormField(
      controller: _description,
      decoration: InputDecoration(
        hintText: 'Role Discription',
        border: const OutlineInputBorder(),
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Please make a discription of your event';
        }
        return null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    //Needed for FutureBuilder after a role is created or Updated
    Future<Role>? _futureRole = useProvider(futureRoleStateProvider).state;

    //A role selected for update
    RoleChangeNotifier _selectedRole = useProvider(myRoleNotifierProvider);

    //Create or update mode
    bool _editMode = useProvider(editModeStateProvider).state;

    //Add selected role in form fields
    context.read(listRoleProvider.notifier).addSelectedRoleInFormFields(
        _roleName, _description, _selectedRole);

    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: (_futureRole == null)
            ? Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 4),
              child: _buildRoleName(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 4),
              child: _buildDescription(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 4),
              child: ElevatedButton(
                onPressed: () {
                  _editMode
                      ? context.read(listRoleProvider.notifier).updateRole(_roleName, _description, _selectedRole)
                      : context.read(listRoleProvider.notifier).createRole(_roleName, _description);
                },
                child: Text(_editMode ? 'Update' : 'Submit'),
              ),
            ),
          ],
        )
            : FutureBuilder<Role>(
          future: _futureRole,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Center(
                child: Container(
                  height: 300,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Icon(
                        Icons.check_circle_outline,
                        color: Colors.green,
                        size: 200,
                      ),
                      Text(
                        _editMode
                            ? '${snapshot.data!.roleName} Updated'
                            : '${snapshot.data!.roleName} Saved',
                        style:
                        TextStyle(fontSize: 24, color: Colors.green),
                      ),
                    ],
                  ),
                ),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}


