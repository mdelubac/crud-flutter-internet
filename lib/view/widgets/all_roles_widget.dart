import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:linking_pictures/viewModel/role_providers.dart';
import 'package:linking_pictures/viewModel/list_role_state_notifier.dart';

class AllRoles extends HookWidget {
  @override
  Widget build(BuildContext context) {

    Future<List<Role>> _futureRoles = useProvider(listRoleProvider);

    return Container(
      child: FutureBuilder<List<Role>>(
        future: _futureRoles,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text("${snapshot.error}");
          } else if (snapshot.connectionState == ConnectionState.done) {
            List<Role> response = snapshot.data!;
            return ListView.builder(
                itemCount: response.length,
                itemBuilder: (context, index) {
                  return Dismissible(
                    key: UniqueKey(),
                    child: Card(
                      elevation: 4,
                      margin: EdgeInsets.all(8),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: ListTile(
                        leading: CircleAvatar(
                          child: Text('${response[index].roleName![0]}'),
                        ),
                        title: Text("${response[index].roleName}"),
                        subtitle: Text("${response[index].description}"),
                        onTap: () => context
                            .read(myRoleNotifierProvider)
                            .selectedRole(response[index]),
                      ),
                    ),
                    background: Container(
                      color: Colors.green,
                      child: Icon(Icons.check),
                    ),
                    secondaryBackground: Container(
                      color: Colors.red,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.cancel_outlined),
                      ),
                    ),
                    onDismissed: (direction) {
                      context
                          .read(listRoleProvider.notifier)
                          .deleteRole(response[index]);
                    },
                  );
                });
          }
          return CircularProgressIndicator();
        },
      ),
    );
  }
}


