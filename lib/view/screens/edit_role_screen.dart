import 'package:flutter/material.dart';
import 'package:linking_pictures/view/widgets/form_add_update_role_widget.dart';

class EditRoleScreen extends StatefulWidget {
  @override
  _EditRoleScreenState createState() => _EditRoleScreenState();
}

class _EditRoleScreenState extends State<EditRoleScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('EditRole'),
      ),
      body: FormAddOrUpdateRole(),
    );
  }
}


