import 'package:flutter/material.dart';
import 'package:linking_pictures/model/role.dart';
import 'package:linking_pictures/view/widgets/all_roles_widget.dart';
import 'package:linking_pictures/view/widgets/form_add_update_role_widget.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Role? role;
  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    AllRoles(),

    FormAddOrUpdateRole(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CRUD role internet'),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
    bottomNavigationBar: BottomNavigationBar(
      items: const<BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home'
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: 'Event'
        ),
      ],
      currentIndex: _selectedIndex,
      type: BottomNavigationBarType.fixed,
      selectedItemColor: Colors.amber[800],
      onTap: _onItemTapped,
    )
    );
  }
}


